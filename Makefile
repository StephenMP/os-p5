
all: buddy list dash

buddy:
	cd buddysrc; make
	cp buddysrc/lib/*.* ./lib/ 
	
list:
	cd listsrc; make
	cp listsrc/lib/*.* ./lib/ 
	
dash:
	cd dashsrc; make; cp ./mydash ../
	
clean:
	cd dashsrc; make clean
	rm -f ./dash
	rm -f lib/*.*
	cd listsrc; make clean
	cd buddysrc; make clean
