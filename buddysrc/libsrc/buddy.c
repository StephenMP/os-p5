#include <stdio.h>
#include <stdlib.h>
#include "buddy.h"

/*******************/
/** Instance Data **/
/*******************/
/* Stores memory initialized */
static void *initialized_mem;
/* Stores the size of memory initialized */
static size_t initialized_size;
/* Stores the power of 2 related to size initialized */
static size_t initialized_pow;
/* Used to track our memory blocks */
static MemBlockPtr blocks[MAX_SIZE];
/* Used to track if buddy_init() has been called */
static Boolean initialized = FALSE;

//********************************//
//** Static (private) functions **//
//********************************//
/**
 * Finds the next power of 2 greater than the size requested
 * (e.g. closest_pow_2(1000) would return 10 as 2^10 is 1024).
 * This way is much faster than using the log base 2 method and
 * does not depend on an external lib.
 *
 * The algorithm is based on an algorithm from:
 * http://www.geeksforgeeks.org/next-power-of-2/
 *
 * @param size_t amount of memory requested
 * @return size_t power of 2 for next highest value
 */
static size_t closest_pow_of_2(size_t sizeAskedFor){
	if(sizeAskedFor < 2)
		return 0;

	uint val, power;

	/* Start at 2^0 */
	val = 1;
	power = 0;

    /* Until we are larger than the memory to allocate */
	while(val < sizeAskedFor)
	{
	    /* Go to the next power of 2 */
		val <<= 1;
		power++;
	}

	return power;
}

/**
 * First checks if we have the the available memory in the kth
 * list. If not, we proceed to traverse the jth list and split
 * memory chunks in half recursively.
 * @param uint the power of 2 currently saught
 * @param uint the power of 2 asked for
 * @return A pointer to the new block post split
 */
static void *split_mem(uint curr, uint closest){
    /* Search in the kth list? */
	if(curr == closest){
        /* Get the available block */
		MemBlockPtr block = blocks[curr];

        /* If not split, we have our block */
		if(block->next == NULL)
			blocks[curr] = NULL;
        /* Otherwise, we get the already split block */
		else{
			blocks[curr] = block->next;
			blocks[curr]->prev = NULL;
		}

		/* Mark it as used */
		block->header.inUse = TRUE;

		/* Track it's power of 2 */
		block->header.memPow = curr;

        /* Get the pointer to our block */
		char * tmpBlock = (char *) block;

		/* Offset our pointer by our header */
		size_t resultMemPtr = (uint) tmpBlock+sizeof(BlockHeader);

		/* Return the new pointer */
		return (void *)(resultMemPtr);
	}

    /* Otherwise, search in the jth list */
	MemBlockPtr mainBlock = blocks[curr];

	/* Same as above */
	if(mainBlock->next == NULL)
		blocks[curr] = NULL;
	else{
		blocks[curr] = (MemBlockPtr) mainBlock->next;
		blocks[curr]->prev = NULL;
	}

    /* Decrement our power of 2 */
	curr--;

	/* Update our pointer to new correct mem location */
	MemBlockPtr newBlock = (MemBlockPtr)((void *) mainBlock + ( 1 << curr));
	/* This block will not be used */
	newBlock->header.inUse = FALSE;
	/* Update it's power of 2 */
	newBlock->header.memPow = curr;
	/* Refer to back to the main block */
	newBlock->prev = mainBlock;
	newBlock->next = NULL;

    /* Update kth list with new block */
	blocks[curr] = mainBlock;
	/* Decrement it's pow of 2 since we split it */
	mainBlock->header.memPow--;
	/* Point to the split block */
	mainBlock->next = newBlock;
	mainBlock->prev = NULL;

    /* Recurse until we get the proper amount of memory */
	return split_mem(curr, closest);
}

/**
 * Used by realloc to copy memory from an old block to
 * a new block.
 * @param newMem Pointer to new memory block
 * @param oldMem Pointer to old memory block
 * @param memSize The size of the old memory block
 */
static void *copy_mem(void* newMem, const void* oldMem, size_t memSize){
    char *nm = (char *) newMem;
    char *om = (char *) oldMem;

    for (int i=0; i<memSize; i++)
        nm[i]=om[i];

    return nm;
}

/**
 * Used by calloc to zero out the memory prior to return
 * @param block The bock pointer to zero
 * @param memSize The size of the block
 */
static void *sanitize_mem(void *block, size_t memSize){
    char *b = (char *)block

    for(i=0; i < memSize; i++)
		blockPtr[i] = (char *)0;

    return b;
}

/**
 * Finds the buddy of a block
 * @param ptr A pointer to a block of memory
 * @param pow The power of two of that block
 * @return A pointer to the block's buddy
 */
static MemBlockPtr find_buddy(MemBlockPtr ptr, uint pow)
{
    size_t position;
    MemBlockPtr curr, tmp;

    curr = blocks[pow];
	while(curr != NULL){
        position = (size_t)ptr - initialized_size;
        tmp = (MemBlockPtr)(initialized_size + ((position ^ 1) << ptr->header.memPow));

		if(curr == ptr)
			return curr;

		curr = curr->next;
	}

	return NULL;
}

/**
 * Merges two buddy blocks of memory recursively
 * @param ptr Pointer to the block to merge
 * @param pow The power of the block merging
 */
static size_t merge_buddies(MemBlockPtr ptr, uint pow)
{
    MemBlockPtr buddy = find_buddy(ptr, pow);

    /* Base case, realign blocks post merging */
    if(buddy == NULL){
        MemBlockPtr tmp, curr;

        /* Get current block */
        curr = blocks[pow];

        /* We may already be in the right place */
        if(curr == NULL){
            curr = ptr;
            ptr->next = NULL;
            ptr->prev = NULL;
            return;
        }

        /* Otherwise, move up the list to find the proper place */
        while(curr != NULL){
            /* We are a sub-block and need to update references */
            if(ptr < curr){
                ptr->prev = curr->prev;
                ptr->next = curr;

                /* Update links if we have a previous link to update */
                if(curr->prev != NULL)
                    curr->prev->next = ptr;

                /* Otherwise, we have arrived */
                else
                    blocks[i] = ptr;

                /* Now update re-assign the link */
                curr->prev = ptr;

                /* Done */
                return;
            }

            /* Otherwise, continue traversing the list */
            tmp = curr;
            curr = curr->next;
        }

        /* We made it to the end, update links */
        tmp->next = ptr;
        ptr->prev = tmp;
        ptr->next = NULL;
    }

    /* Otherwise, recursively merge buddies */
    else{
        int cPow;

        /* Get correct order */
        if(buddy < ptr){
            MemBlockPtr tmp;

            tmp = buddy;
            buddy = ptr;
            ptr = tmp;
        }

        /* Get the size */
        cPow = ptr->header.memPow;

        /* Bump the power of two pre-merge */
        ptr->header.memPow++;
        rightBlock->header.memPow++;

        /* Check child list */
        if(buddy->prev == NULL){
            /* No children, so we are good to go */
            if(buddy->next == NULL)
                blocks[cPow] = NULL;

            /* Otherwise, we need to update links */
            else{
                blocks[cPow] = buddy->next;
                buddy->next->prev = NULL;
            }
        }

        /* There are child links */
        else{
            /* Are the links one way? */
            if(buddy->next == NULL)
                buddy->prev->next = NULL;
            else{
                buddy->prev->next = buddy->next;
                buddy->next->prev = buddy->prev;
            }
        }

        /* Next */
        merge_buddies(ptr, cPow);
    }
}

/**
 * Used to throw a memory error. Sets errno to ENOMEM
 * as required by the project specs.
 */
static void throw_mem_err() {
	errno = ENOMEM;
	exit(errno);
}
/* Private functions */

//**********************//
//** Public functions **//
//**********************//
void buddy_init(size_t powOfTwo)
{
    /* Check corner cases */
    if(n > MAX_MEM_SUPPORT)
        throw_mem_err();
	else if(n == 0)
        n = DEFAULT_MEM_SIZE

    /* Get the next closest power of 2 */
	initialized_pow = closest_pow_of_2(n);

	/* Grab our memory using sbrk() */
	initialized_mem = sbrk(1 << initialized_pow);
	initialized_size = (size_t) initialized_mem;

    /* Reassign the pointer to be our memory block */
    blocks[initialized_pow] = (MemBlockPtr) initialized_mem;
	/* Memory is not yet in use */
	blocks[initialized_pow]->header.inUse = FALSE;
	/* Tack how big the block of memory is */
	blocks[initialized_pow]->header.memPow = initialized_pow;
    /* Set our next/prev pointers to null for in use/buddy tracking */
	blocks[initialized_pow]->next = NULL;
	blocks[initialized_pow]->prev = NULL;

    /* We have successfully initialized */
    initialized = TRUE;
}

/** Interpose on calloc */
void *calloc(size_t nmemb, size_t size)
{
    buddy_calloc(nmemb, size);
}

void *buddy_calloc(size_t nmemb, size_t size)
{
    /* Return null if no members as per man page of calloc */
    if(nmemb == 0)
        return NULL;

    size_t closest, totalMemory;
	int i;

    /* Get the total memory we need */
    totalMemory = nmemb * size;

    /* Find the closest higher power of 2 */
	closest = closest_pow_of_2(totalMemory);

    /* Use malloc to get our memory */
	void *blockPtr = buddy_malloc(closest);

    /* Sanitize it by setting all blocks to 0 */
    sanitize_mem(blockPtr, closest);

	return blockPtr;
}

/** Interpose on malloc */
void *malloc(size_t size)
{
    buddy_malloc(size);
}

void *buddy_malloc(size_t size)
{
    /* Account for our header */
    size += sizeof(MemHeader);

    /* Check corner cases */
    if(size > MAX_MEM_SUPPORT)
        throw_mem_err();

    /* Call buddy_init if not initialized */
    if(initialized == FALSE)
        buddy_init((size_t)MAX_MEM_SUPPORT);

    /* Go up to the closest higher power of 2 */
    uint closest, curr;
    closest = closest_pow_of_2(size);
    curr = closest;

    /* Continue to iterate mem lists */
	while (curr <= initialized_pow){
        /* If we have an available memory block here */
		if(blocks[curr] != NULL){
            /* Setup pointer */
			void *blockPtr;

            /* Get our block */
			blockPtr = split_mem(curr, closest);

            /* Throw error if we can't get memory */
			if(blockPtr == NULL)
				throw_mem_err();

            /* Otherwise, give back the pointer to the memory */
			else
				return blockPtr;
		}

        /* Otherwise, traverse the list of blocks */
		curr++;
	}

	return NULL;
}

/** Interpose on realloc */
void *realloc(void *ptr, size_t size){
    buddy_realloc(ptr, size);
}

void *buddy_realloc(void *ptr, size_t size)
{
    void *newBlock

    /* As per man page, should act like free */
    if(ptr != NULL && size == 0){
        buddy_free(ptr);
        return NULL;
    }

    /* Get new memory */
    newBlock = buddy_malloc(size);

    /* As per man page, should act like malloc */
    if(ptr == NULL)
        return newBlock;

    /* Get pointer to the old block of of memory */
    MemBlockPtr oldBlock = (MemBlockPtr) ((char *)ptr - sizeof(MemHeader));

    /* Copy the data from the old block to the new block */
    copy_mem(newBlock, ptr, ((size_t) oldBlock - sizeof(MemBlockPtr)));

    /* Free the old memory */
    buddy_free(ptr);
}

/** Interpose on free */
void free(void *obj)
{
    buddy_free(obj);
}

void buddy_free(void *obj)
{
    /* As per man page, no operation should occur */
    if(obj == NULL)
        return;

    /* Get pointer to the block of memory */
    MemBlockPtr block = (MemBlockPtr) ((char *)obj - sizeof(MemHeader));

    uint blockPow;
    blockPow = block->header.memPow;

    /* Reset values */
	block->header.inUse = FALSE;
	block->prev = NULL;
	block->next = NULL;

	/* Determine if we need to merge */
	if(blocks[blockPow] == NULL)
        blocks[blockPow] = block)
    else
        merge_buddies(block, blockPow);
}
/* Public functions */
