#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "common.h"

#ifndef BUDDY_H_
#define BUDDY_H_

#ifndef MAX_MEM_SIZE
#define MAX_MEM_SIZE 34359738368
#endif

#ifndef MAX_MEM_SUPPORT_POW
#define MAX_MEM_SUPPORT_POW 35
#endif

#ifndef DEFAULT_MEM_SIZE
#define DEFAULT_MEM_SIZE 536870912
#endif

#ifndef DEFAULT_MEM_POW
#define DEFAULT_MEM_POW 29
#endif

/**
 * Struct to represent a block header
 */
typedef struct mem_header MemHeader;
struct mem_header{
	Boolean inUse;
	uint memPow;
};

/**
 * Basically a list of headers to allow us to traverse
 * headers as necessary by the buddy algorithm.
 */
typedef struct mem_block MemBlock;
typedef struct mem_block * MemBlockPtr;
struct mem_block{
    /* Will track the TAG field */
	MemHeader header;
	/* Link A */
	MemBlockPtr next;
	/* Link B */
	MemBlockPtr prev;
};

//TODO: Need to also figure out how split/merge and other static methods will
// behave and what params they take, then add method pre-processors here

/**
 * Initializes buddy allocator with base memory passed by size_t
 * as the amount of memory. size_t will be rounded up to the nearest
 * power of 2 (e.g. buddy_init(1000) will result in 1024 (2^10) bytes
 * of memory initialized. Max memory supported is 32GB (2^35). Passing
 * 0 will result in a default of 512MB (2^29) bytes initialized.
 * @param size_t the memory to allocate represented as a power of 2
 */
void buddy_init(size_t);

/**
 * Replaces the normal calloc with our buddy allocation algorithm.
 * If buddy_init() has not been called, it will be called under the
 * hood.
 * @param size_t # of members to allocate for
 * @param size_t size of members
 */
void *buddy_calloc(size_t, size_t);

/**
 * Replaces the normal malloc with our buddy allocation algorithm
 * If buddy_init() has not been called, it will be called under the
 * the hood.
 * @param size_t Total memory to be allocated
 */
void *buddy_malloc(size_t);

/**
 * Replaces the normal system realloc with our buddy allocation algorithm.
 * buddy_init() MUST have been called prior.
 * @param void *ptr the pointer to the block of memory
 * @param size_t size the size of memory to re-allocate
 */
void *buddy_realloc(void *ptr, size_t size);

/**
 * Replaces the normal system free with our buddy allocation algorithm.
 * buddy_init() MUST have been called to allocate this memory
 * @param void * the ptr to the block of memory to free
 */
void buddy_free(void *);

#endif
